
//  Created by Christopher on 9/4/15.

#import <UIKit/UIKit.h>
#import <React/RCTView.h>
#import <React/RCTBridgeModule.h>

@interface RNPrint : RCTView <RCTBridgeModule, UIPrintInteractionControllerDelegate, UIPrinterPickerControllerDelegate>
@property UIPrinter *pickedPrinter;
@property NSString *filePath;
@property NSString *htmlString;
@property NSString *lines;
@property NSString *font;
@property NSURL *printerURL;
@property (nonatomic, assign) BOOL isLandscape;
@property (nonatomic, assign) NSInteger fontSize;
@property (nonatomic, assign) NSInteger offset;
@property (nonatomic, assign) NSInteger xoffset;
@end
