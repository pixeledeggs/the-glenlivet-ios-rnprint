type PrintOptionsType = {
	printerURL?: string;
	isLandscape?: boolean;
	font?: string;
	fontSize?: integer;
	offset?: integer;
	xoffset?: integer;
	jobName?: string;				
} & ({ lines: string } | { filePath: string });

type SelectPrinterOptionsType = {
	x: string;
	y: string;
};

export function print(options: PrintOptionsType): Promise<any>;

export function selectPrinter(options: SelectPrinterOptionsType): Promise<any>;
